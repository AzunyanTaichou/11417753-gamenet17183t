﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class Player : NetworkBehaviour
{
    public GameObject Projectile;
    public Transform ProjectileSpawn;
    public float Speed;
    public float JumpForce = 300;
    public float RateOfFire = 0.4f;

    private bool canDoubleJump = true;
    private float direction;
    private Animator animator;
    private Rigidbody2D rigidbody;
    private Vector3 scale;
    private bool facingRight = true;

   
    private float elapsedTime;

    // Use this for initialization
    void Start ()
    {
        scale = transform.localScale;
        animator = GetComponent<Animator>();
        rigidbody = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        if (!isLocalPlayer) return;

        CheckAnimationState();
        Jump();
        Move();

        
        if (Input.GetMouseButtonDown(0) && RateOfFire <= elapsedTime)
        {
            CmdShoot();
        }
        
        // Update Time
        elapsedTime += Time.deltaTime;
    }

    void Move()
    {
        direction = Input.GetAxis("Horizontal");

        // Moves the player
        rigidbody.velocity = new Vector3(direction * Speed, rigidbody.velocity.y);

        // Checks direction and determines if the sprite should flip or not
        if (direction > 0 && scale.x == -1)
        {
            scale.x = -scale.x;
            facingRight = true;
        }
        else if (direction < 0 && scale.x == 1)
        {
           scale.x = -scale.x;
           facingRight = false;
        }
        transform.localScale = scale;
    }

    void Jump()
    {
        // Checks if player presses space and player's y axis is at 0
        if (Input.GetKeyDown(KeyCode.Space))
        {
            if (rigidbody.velocity.y == 0)
            {
                rigidbody.AddForce(Vector2.up * JumpForce);
                canDoubleJump = true; // lets player double jump
            }

            // If player double jumps, sets double jump to used
            else if (rigidbody.velocity.y > 0 && canDoubleJump)
            {
                canDoubleJump = false;
                rigidbody.AddForce(Vector2.up * JumpForce);
            }
        }
    }

    [Command]
    void CmdShoot()
    {
       
        // Checks if player presses shoot and if player is able to shoot
        
        animator.SetTrigger("Shoot");
        elapsedTime = 0.0f; // Resets the cooldown
            
        // Shoot Right
        if (facingRight) 
        {
            var projectileInstance = (GameObject)Instantiate(Projectile, ProjectileSpawn.position, ProjectileSpawn.localRotation);  // Creates a projectile, spawning off of gun's location
            projectileInstance.GetComponent<Rigidbody2D>().velocity = projectileInstance.transform.right * Projectile.GetComponent<Projectile>().Speed;  // Creates velocity by getting the Speed from Projectile script
            NetworkServer.Spawn(projectileInstance);
        }
        // Shoot Left
        else
        {
            var projectileInstance = (GameObject)Instantiate(Projectile, ProjectileSpawn.position, Quaternion.Euler(0, 0, -180));
            projectileInstance.GetComponent<Rigidbody2D>().velocity =  projectileInstance.transform.right * Projectile.GetComponent<Projectile>().Speed;
            NetworkServer.Spawn(projectileInstance);
        }  
    }

    void CheckAnimationState()
    {
        // If player is moving play move animation
        if (rigidbody.velocity.x > 0 || rigidbody.velocity.x < 0) animator.SetFloat("Speed", Mathf.Abs(rigidbody.velocity.x));

        // If player is jumping play jump animation
        if (rigidbody.velocity.y > 0 || rigidbody.velocity.y < 0) animator.SetBool("isJumping", true);

        // If player is no longer jumping return to idle state
        if (rigidbody.velocity.y == 0) animator.SetBool("isJumping", false);

        // If player is Double jumping
        if ((rigidbody.velocity.y > 0 || rigidbody.velocity.y < 0) && canDoubleJump == false) animator.SetBool("isDoubleJumping", true);

        // If player is no longer Double jumping
        if (rigidbody.velocity.y == 0) animator.SetBool("isDoubleJumping", false);
    }
}
