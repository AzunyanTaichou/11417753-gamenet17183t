﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class Projectile : NetworkBehaviour
{
    public float Speed = 8;
    public float Duration = 2.5f;
    public float KnockBack = 5.0f;
    // Use this for initialization

    void Start()
    {
        Destroy(gameObject, Duration); // destroys projectile after its duration
    }

    void OnTriggerEnter2D(Collider2D collision)
    {
        // Gets the player
        Player player = collision.gameObject.GetComponent<Player>();
        
        // If the projectile hits an entity knockback
        if (player)
        {
           Vector2 direction = (player.transform.position - gameObject.transform.position).normalized;
           player.GetComponent<Rigidbody2D>().velocity = new Vector2(KnockBack * direction.x, 0);

           Destroy(gameObject); // Destroy the projectile
          
        }
        else
        {
            Destroy(gameObject);
        }
    }
}